import React from 'react';
import classes from './AnswerItem.scss';

const AnswerItem = props => {
  const classesAnswerItem = [classes.AnswerItem];

  if (props.state) {
    classesAnswerItem.push(classes[props.state]);
  }

  return (
      <li className={classesAnswerItem.join(' ')}
          onClick={() => {props.onAnswerClick(props.answer.id);}}>
        {props.answer.text}
      </li>
  );
};

export default AnswerItem;
