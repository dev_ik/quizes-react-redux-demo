import React from 'react';
import classes from './MenuToggle.scss';

const MenuToggle = props => {

  const classesMenuToggle = [
    classes.MenuToggle,
    'fa',
  ];

  if (props.isOpen) {
    classesMenuToggle.push('fa-times');
    classesMenuToggle.push(classes.open);
  } else {
    classesMenuToggle.push('fa-bars');
  }

  return (
      <i
          className={classesMenuToggle.join(' ')}
          onClick={props.onToggle}
      />
  );
};

export default MenuToggle;
