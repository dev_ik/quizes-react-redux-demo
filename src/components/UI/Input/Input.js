import React from 'react';
import classes from './Input.scss';

function isInvalid(props) {
  return !props.valid && props.shouldValidate && props.touched;
}

const Input = props => {
  const inputType = props.type || 'text';
  const classesInput = [classes.Input];
  const htmlFor = `${inputType}-${Math.random()}`;

  if (isInvalid(props)) {
    classesInput.push(classes.invalid);
  }

  return (
      <div className={classesInput.join('  ')}>

        <label htmlFor={htmlFor}>{props.label}</label>
        <input
            id={htmlFor}
            type={inputType}
            value={props.value}
            onChange={props.onChange}
        />

        {isInvalid(props)
            ? <span>{props.errorMessage || 'wrong value'}</span>
            : null}

      </div>
  );
};

export default Input;
