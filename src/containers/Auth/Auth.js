import React from 'react';
import classes from './Auth.scss';
import Button from '../../components/UI/Button/Button';
import Input from '../../components/UI/Input/Input';
import {validate, validateForm} from '../../form/form';
import {connect} from 'react-redux';
import {auth} from '../../store/actions/authActions';

class Auth extends React.Component {
  state = {
    isFormValid: false,
    formControls: {
      email: {
        value: '',
        type: 'email',
        label: 'Email',
        errorMessage: 'Email not correct',
        valid: false,
        touched: false,
        validation: {
          required: true,
          email: true,
        },
      },
      password: {
        value: '',
        type: 'password',
        label: 'Password',
        errorMessage: 'Password not correct',
        valid: false,
        touched: false,
        validation: {
          required: true,
          minLength: 6,
        },
      },
    },
  };

  loginHandler = () => {

    this.props.auth(
        this.state.formControls.email.value,
        this.state.formControls.password.value,
        true,
    );
  };

  registerHandler = () => {
    this.props.auth(
        this.state.formControls.email.value,
        this.state.formControls.password.value,
        false,
    );
  };

  submitHandler(event) {
    event.preventDefault();
  }

  onChangeHandler(event, controlName) {
    const formControls = {...this.state.formControls};
    const control = {...formControls[controlName]};

    control.value = event.target.value;
    control.touched = true;
    control.valid = validate(control.value, control.validation);
    formControls[controlName] = control;

    this.setState({
      isFormValid: validateForm(formControls),
      formControls,
    });
  }

  renderInputs() {
    return Object.keys(this.state.formControls)
    .map((controlName, index) => {
      const control = this.state.formControls[controlName];
      return (
          <Input
              key={controlName + index}
              type={control.type}
              value={control.value}
              valid={control.valid}
              touched={control.touched}
              label={control.label}
              shouldValidate={!!control.validation}
              errorMessage={control.errorMessage}
              onChange={event => this.onChangeHandler(event, controlName)}
          />
      );
    });
  }

  render() {
    return (
        <div className={classes.Auth}>
          <div>
            <h1>Authorization</h1>

            <form onSubmit={this.submitHandler}>

              {this.renderInputs()}
              <Button
                  type="success"
                  onClick={this.loginHandler}
                  disabled={!this.state.isFormValid}
              >
                Log In
              </Button>
              <Button
                  type="primary"
                  onClick={this.registerHandler}
              >
                Register
              </Button>
            </form>
          </div>
        </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    auth: (email, password, isLogin) => dispatch(auth(email, password, isLogin)),
  };
};

export default connect(null, mapDispatchToProps)(Auth);
