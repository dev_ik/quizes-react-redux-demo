import React from 'react';
import classes from './QuizCreator.scss';
import Button from '../../components/UI/Button/Button';
import {createControl, validate, validateForm} from '../../form/form';
import Input from '../../components/UI/Input/Input';
import Select from '../../components/UI/Select/Select';
import {connect} from 'react-redux';
import {
  createQuizQuiestion,
  finishCreateQuiz,
} from '../../store/actions/createActions';

function createOptionControl(number) {
  return createControl({
    label: `Option ${number}`,
    errorMessage: 'Option cannot be empty',
    id: number,
  }, {required: true});
}

function createFormControls() {
  return {
    question: createControl({
      label: 'Question',
      errorMessage: 'Question cannot be empty',
    }, {required: true}),
    option1: createOptionControl(1),
    option2: createOptionControl(2),
    option3: createOptionControl(3),
    option4: createOptionControl(4),
  };
}

class QuizCreator extends React.Component {

  state = {
    isFormValid: false,
    rightAnswerId: 1,
    formControls: createFormControls(),
  };

  onSubmitHandler(event) {
    event.preventDefault();
  }

  addQuestionHandler = (event) => {
    event.preventDefault();
    const {question, option1, option2, option3, option4} = this.state.formControls;

    const quiestionItem = {
      question: question.value,
      id: this.props.quiz.length + 1,
      rightAnswerId: this.state.rightAnswerId,
      answers: [
        {text: option1.value, id: option1.id},
        {text: option2.value, id: option2.id},
        {text: option3.value, id: option3.id},
        {text: option4.value, id: option4.id},
      ],
    };

    this.setState({
      rightAnswerId: 1,
      isFormValid: false,
      formControls: createFormControls(),
    });

    this.props.createQuizQuestion(quiestionItem);

  };

  createQuizHandler = event => {
    event.preventDefault();

    this.setState({
      isFormValid: false,
      rightAnswerId: 1,
      formControls: createFormControls(),
    });

    this.props.finishCreateQuiz();
  };

  onChangeHandler = (value, controlName) => {
    const formControls = {...this.state.formControls};
    const control = {...formControls[controlName]};

    control.touched = true;
    control.value = value;
    control.valid = validate(control.value, control.validation);

    formControls[controlName] = control;

    this.setState({
      isFormValid: validateForm(formControls),
      formControls,
    });

  };

  renderFormControls() {
    return Object.keys(this.state.formControls)
    .map((controlName, index) => {
      const control = this.state.formControls[controlName];

      return (
          <React.Fragment key={index}>
            <Input
                label={control.label}
                value={control.value}
                valid={control.valid}
                shouldValidate={!!control.validation}
                touched={control.touched}
                errorMessage={control.errorMessage}
                onChange={event => this.onChangeHandler(event.target.value,
                    controlName)}
            />
            {index === 0 ? <hr/> : null}
          </React.Fragment>
      );
    });
  }

  onSelectChangeHandler = (event) => {
    this.setState({
      rightAnswerId: +event.target.value,
    });
  };

  render() {
    const select = <Select
        label="Select right answer"
        value={this.state.rightAnswerId}
        onChange={this.onSelectChangeHandler}
        options={[
          {text: '1', value: 1},
          {text: '2', value: 2},
          {text: '3', value: 3},
          {text: '4', value: 4},
        ]}
    />;

    return (
        <div className={classes.QuizCreator}>
          <div>
            <h1>Create quiz</h1>
            <form onSubmit={this.onSubmitHandler}>

              {this.renderFormControls()}

              {select}

              <Button
                  type='primary'
                  onClick={this.addQuestionHandler}
                  disabled={!this.state.isFormValid}
              >
                add question
              </Button>

              <Button
                  type='success'
                  onClick={this.createQuizHandler}
                  disabled={this.props.quiz.length === 0}
              >
                create quiz
              </Button>

            </form>
          </div>
        </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    quiz: state.create.quiz,
  };
};

const maptDispatchToProps = dispatch => {
  return {
    createQuizQuestion: item => dispatch(createQuizQuiestion(item)),
    finishCreateQuiz: () => dispatch(finishCreateQuiz()),
  };
};

export default connect(mapStateToProps, maptDispatchToProps)(QuizCreator);
