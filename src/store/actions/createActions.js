import {CREATE_QUIZ_QUESTION, RESET_QUIZ_CREATION} from './actionsType';
import axios from '../../axios/axios.quiz';

export function createQuizQuiestion(item) {
  return {
    type: CREATE_QUIZ_QUESTION,
    payload: {
      item,
    },
  };
}

export function finishCreateQuiz() {
  return async (dispatch, getState) => {
    debugger;
    await axios.post('/quizes.json', getState().create.quiz);
    dispatch(resetQuizCreation())
  };
}

export function resetQuizCreation() {
  return {
    type: RESET_QUIZ_CREATION,
  };
}
